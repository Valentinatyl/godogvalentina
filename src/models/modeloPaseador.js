const mongoose = require("mongoose");

const paseador = new mongoose.Schema({

    nombre:{
        type:String,
        required: true,
    },

    documento: {
        type: String,
        required: true,
    },

    edad:{
        type: String,
        required: true,
    },

    email:{
        type: String,
        required: true,
    },
});

//Crea el modelo de paseador GoDog utilizando el esquema definido

const Paseador = mongoose.model("paseadores",paseador);

//Exporta el modelo para que esté disponible en otros archivos

module.exports = Paseador;