const express = require("express");

const Paseador = require("../models/modeloPaseador.js");

const rutas = express.Router();

const connectDB = require("../connection/db.js");
connectDB();

//Traer información

rutas.get("/paseadores",async(req,res)=>{
    const paseadores = await Paseador.find(); //Busca todos los paseadores en la base de datos 
    res.json(paseadores); //Devuelve la lista de paseadores en formato JSON
});

rutas.post("/paseadores",async(req,res)=>{
    
    const {nombre,documento,edad,email} = req.body;

    const nuevoPaseador = new Paseador({
        nombre,
        documento,
        edad,
        email
    });

    nuevoPaseador.nombre = nombre;
    nuevoPaseador.documento = documento;
    nuevoPaseador.edad = edad;
    nuevoPaseador.email = email;

    await nuevoPaseador.save();
    res.json({"mensaje":"Paseador guardado con éxito"});
});

rutas.put("/paseadores/:id",async(req,res)=>{
    const paseadorid = req.params.id;
    const {nombre,documento,edad,email}=req.body
    const pasedorEncontrado = await Paseador.findById(paseadorid);

    pasedorEncontrado.nombre = nombre;
    pasedorEncontrado.documento = documento;
    pasedorEncontrado.edad = edad;
    pasedorEncontrado.email = email;

    await pasedorEncontrado.save();
    res.json(pasedorEncontrado);
});

rutas.delete("/paseadores/:id",async(req,res)=>{
    const paseadorid = req.params.id;
    const paseadorBuscado = await Paseador.findById(paseadorid);
    await paseadorBuscado.deleteOne();
    res.json("Paseador eliminado");
});

module.exports=rutas;