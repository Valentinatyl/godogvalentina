const express = require("express");

const app = express();

const config = require("./src/config/config.js");

const rutasPaseadores = require("./src/router/router.js");

//const puerto = config.server.port || process.env.PORT || 5000

app.use(express.json());

app.use("/apiPaseadores",rutasPaseadores);

app.listen(config.server.port,()=>{
    console.log("servidor funcionando por el puerto " + config.server.port);
});






